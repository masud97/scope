<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scope;

class Database extends Model
{
    public $timestamps = false;
    protected $table = 'databases';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'created_at',
        'updated_at'
    ];

    /**
     * Get the scope associated with the Database.
     */
    public function scope()
    {
        return $this->hasOne(Scope::class);
    }
}
