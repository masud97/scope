<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scope;

class ContentManagementSystem extends Model
{
    public $timestamps = false;
    protected $table = 'content_management_systems';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'created_at',
        'updated_at'
    ];

    /**
     * Get the scope associated with the Content Management System.
     */
    public function scope()
    {
        return $this->hasOne(Scope::class);
    }
}
