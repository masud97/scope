<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scope;

class Type extends Model
{
    public $timestamps = false;

    protected $table = 'types';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'created_at',
        'updated_at'
    ];

    /**
     * Get the scope associated with the Type.
     */
    public function scope()
    {
        return $this->hasOne(Scope::class);
    }

}
