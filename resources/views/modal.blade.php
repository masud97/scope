<!-- Modal for view profile -->
<div class="modal fade" id="profile" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">            
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-user"></i> My Profile </div>                            
                            </div>
                            <div class="portlet-body">
                                <div class="table-scrollable">
                                    <table class="table table-bordered table-hover">
                                        
                                        <tbody>
                                            <tr class="active">
                                                <th> Full Name </th>
                                                <td> {{ Auth::user()->name }} </td>
                                            </tr>
                                            <tr class="success">
                                            <th> User Name </th>
                                            <td> {{ Auth::user()->username }} </td>
                                            </tr>
                                            <tr class="danger">
                                                <th> Email </th>
                                                <td> {{ Auth::user()->email }} </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div> 
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                <a class="btn green" data-toggle="modal" href="#changeProfile" onclick="hideProfileModal();">Update Profile</a>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- /. Modal for view profile -->

<!-- Modal for view update profile -->
<div class="modal fade" id="changeProfile" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">            
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <div class="portlet box blue ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-user"></i> Update Profile Info </div>                                
                            </div>
                            <div class="portlet-body form">
                                <form role="form" action="{{route('user.profile.update',Auth::user()->id)}}" method="POST">
                                    @csrf
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label">Full Name</label>
                                            <div class="input-icon right">
                                                <input type="text" name="name" value="{{Auth::user()->name}}" class="form-control" required="required">
                                                @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror 
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">User Name</label>
                                            <div class="input-icon right">
                                                <input type="text" name="username" value="{{Auth::user()->username}}" class="form-control" required="required"> 
                                                @error('username')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Email</label>
                                            <div class="input-icon right">                                                
                                                <input type="text" name="email" value="{{Auth::user()->email}}" class="form-control" required="required"> 
                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions right">
                                        <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                                        <button type="submit" name="submit" class="btn green">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>                        
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div> 
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- /.Modal for view upate profile -->

<!-- Modal for change password -->
<div class="modal fade" id="changePassword" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Change password</h4>
            </div>
            <div class="modal-body">     
                <form role="form" action="{{route('user.change.password',Auth::user()->id)}}" method="POST">
                    @csrf
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Old Password</label>
                            <div class="input-icon right">
                                <input type="password" name="oldpassword" class="form-control" required="required">
                                @error('oldpassword')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">New Password</label>
                            <div class="input-icon right">
                                <input type="password" name="newpassword" class="form-control" required="required"> 
                                @error('newpassword')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Confirm Password</label>
                            <div class="input-icon right">                                                
                                <input type="password" name="confpassword" class="form-control" required="required"> 
                                @error('confpassword')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-actions right">
                        <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                        <button type="submit" name="submit" class="btn green">Submit</button>
                    </div>
                </form>           
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- /.Modal for change password -->

<!-- Modal for scope details -->
<div class="modal fade" id="scopeDetails" tabindex="-1" role="scopeDetails" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">            
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-user"></i> Scope Details 
                                </div>                           
                            </div>
                            <div class="portlet-body">
                                <div class="table-scrollable">
                                    <table class="table table-bordered table-hover">
                                        
                                        <tbody>
                                            <tr>
                                                <th> Type </th>
                                                <td id="showType"></td>
                                            </tr>
                                            <tr>
                                                <th> Identifier </th>
                                                <td id="showIdentifier"></td>
                                            </tr>
                                            <tr>
                                                <th> Language </th>
                                                <td id="showLanguage"></td>
                                            </tr>

                                            <tr>
                                                <th> Framework </th>
                                                <td id="showFramework"></td>
                                            </tr>
                                            <tr>
                                                <th> Cloud and Infrastructure </th>
                                                <td id="showCloud"></td>
                                            </tr>
                                            <tr>
                                                <th> Database </th>
                                                <td id="showDatabase"></td>
                                            </tr>

                                            <tr>
                                                <th> Content Management System </th>
                                                <td id="showCMS"></td>
                                            </tr>
                                            <tr>
                                                <th> Instructions </th>
                                                <td id="showInstructions"></td>
                                            </tr>
                                            <tr>
                                                <th> Reference </th>
                                                <td id="showReference"></td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                    </div> 
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close Modal</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- /. Modal for view scope details -->

<!-- Modal for scope update -->
<div class="modal fade" id="scopeUpdate" tabindex="-1" role="scopeUpdate" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Update Scope</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE FORM PORTLET-->
                        <div class="portlet light bordered">                                    
                            <div class="portlet-body form">
                                <form role="form" action="{{route('scope.update')}}" method="POST">
                                    @csrf
                                    <div class="form-body">
                                    <div class="scopeId" id="scopeId"></div>
                                    <div class="form-group">
                                        <label>Type <span class="required" aria-required="true"> * </span></label>
                                        <select class="form-control types" name="type" id="typeInUpdate" rqequred="required">
                                            
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Identifier <span class="required" aria-required="true"> * </span></label>  
                                        <div class="identifierInUpdate" id="identifierInUpdate"></div>
                                    </div>

                                    <div class="form-group">
                                        <label>Coding Language</label>
                                        <div class="input-icon">
                                            <i class="fa fa-search"></i>                                        
                                            <select name="language" class="form-control codingLanguages" id="codingLanguageInUpdate">
                                            
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Framework</label>
                                        <div class="input-icon">
                                            <i class="fa fa-search"></i>                                        
                                            <select name="framework" class="form-control frameworks" id="frameworkInUpdate">                                        
                                            
                                            </select> 
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Cloud and Infrastructure</label>
                                        <div class="input-icon">
                                            <i class="fa fa-search"></i>
                                            <select name="cloud" class="form-control cloudAndInfrastructures" id="cloudAndInfrastructureInUpdate">
                                            
                                        </select> </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Database</label>
                                        <div class="input-icon">
                                            <i class="fa fa-search"></i>                                        
                                            <select name="database" class="form-control databases" id="databaseInUpdate">
                                            
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Content Management System</label>
                                        <div class="input-icon">
                                            <i class="fa fa-search"></i>
                                            <select name="cms" class="form-control cms" id="cmsInUpdate">
                                            
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Instructions</label>
                                        <div class="instructionsInUpdate" id="instructionsInUpdate"></div>
                                    </div>

                                    <div class="form-group">
                                        <label>Reference</label>
                                        <div class="referenceInUpdate" id="referenceInUpdate"></div>                                      
                                    </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" name="submit" class="btn blue">Update</button>
                                        <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->                                
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- /.Modal for scope update -->