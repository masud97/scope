<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Brian2694\Toastr\Facades\Toastr;
use App\Scope;
use App\CloudAndInfrastructure;
use App\ContentManagementSystem;
use App\Database;
use App\Framework;
use App\Language;
use App\Type;

class ScopeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'type' => 'required',
            'identifier' => 'required'
                ], [
            'type.required' => 'Type is required.',
            'identifier.required' => 'Identifier is required.'
        ]);

        $data = array(
            'user_id' => Auth::user()->id,
            'type_id' => $request->input('type'),
            'identifier' => $request->input('identifier'),
            'language_id' => $request->input('language'),
            'framework_id' => $request->input('framework'),
            'cloud_id' => $request->input('cloud'),
            'database_id' => $request->input('database'),
            'cms_id' => $request->input('cms'),
            'instructions' => $request->input('instructions'),
            'reference' => $request->input('reference'),
            'created_at' => date('Y-m-d')
        );
        $result = Scope::create($data);
        if($result){
            Toastr::success('New scope created.', 'success');
            return redirect()->back(); 
        }else{
            Toastr::warning('W00ps! Something went wrong. Try again.', 'warning');
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $authId = Auth::user()->id;
        $details = Scope::with('cloud', 'cms', 'database', 'framework', 'language', 'type')->findOrFail($id);
        if($authId == $details->user_id){
            return response()->json(['details' => $details], 200);
        }else{
            return response()->json(['errors' => 'Access Denied'], 200);
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $details = Scope::with('cloud', 'cms', 'database', 'framework', 'language', 'type')->findOrFail($id);
        
        $languageId = $details->language_id;
        $allTypes = Type::all();
        $allLanguages = Language::all();
        $frameworks = Framework::where('language_id', $languageId)->get();
        $allClouds = CloudAndInfrastructure::all();
        $allDatabases = Database::all();
        $allCMS = ContentManagementSystem::all();


        return response()->json(['details' => $details, 'allTypes' => $allTypes, 'allLanguages' => $allLanguages, 'frameworks' => $frameworks, 'allClouds' => $allClouds, 'allDatabases' => $allDatabases, 'allCMS' => $allCMS], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'type' => 'required',
            'identifier' => 'required'
                ], [
            'type.required' => 'Type is required.',
            'identifier.required' => 'Identifier is required.'
        ]);

        $scopeId = $request->input('scopeid');

        if($request->input('instructions') == 'null'){
            $instructions = '';
        }else{
            $instructions = $request->input('instructions');
        }
        if($request->input('reference') == 'null'){
            $reference = '';
        }else{
            $reference = $request->input('reference');
        }

        $data = array(
            'user_id' => Auth::user()->id,
            'type_id' => $request->input('type'),
            'identifier' => $request->input('identifier'),
            'language_id' => $request->input('language'),
            'framework_id' => $request->input('framework'),
            'cloud_id' => $request->input('cloud'),
            'database_id' => $request->input('database'),
            'cms_id' => $request->input('cms'),
            'instructions' => $instructions,
            'reference' => $reference,
            'updated_at' => date('Y-m-d')
        );

        $result = Scope::where('id', $scopeId)->update($data);
        if($result){
            Toastr::success('Scope updated succefully.', 'success');
            return redirect()->back(); 
        }else{
            Toastr::warning('W00ps! Something went wrong. Try again.', 'warning');
            return redirect()->back();
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $scope = Scope::findOrFail($id);
        $result = $scope->delete();

        if($result){
            Toastr::success('Scope has deleted successfully.', 'success');
            return redirect()->back();
        }else{
            Toastr::warning('W00ps! Something went wrong. Try again.', 'warning');
            return redirect()->back();
        }   
    }
}
