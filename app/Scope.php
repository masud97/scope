<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CloudAndInfrastructure;
use App\ContentManagementSystem;
use App\Database;
use App\Framework;
use App\Language;
use App\Type;
use App\User;

class Scope extends Model
{
    public $timestamps = false;
    protected $table = 'scopes';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'type_id',
        'identifier',
        'language_id',
        'framework_id',
        'cloud_id',
        'database_id',
        'cms_id',
        'instructions',
        'reference',
        'created_at',
        'updated_at',
        'deleted_at',
        'status'  
    ];


    /**
     * Get the scope that owns the cloud and infrastructure.
     */
    public function cloud()
    {
        return $this->belongsTo(CloudAndInfrastructure::class);
    }

    /**
     * Get the scope that owns the content management system.
     */
    public function cms()
    {
        return $this->belongsTo(ContentManagementSystem::class);
    }

    /**
     * Get the scope that owns the database.
     */
    public function database()
    {
        return $this->belongsTo(Database::class);
    }

    /**
     * Get the scope that owns the framework.
     */
    public function framework()
    {
        return $this->belongsTo(Framework::class);
    }

    /**
     * Get the scope that owns the language.
     */
    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    /**
     * Get the scope that owns the type.
     */
    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    /**
     * Get the scope that owns the user.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
