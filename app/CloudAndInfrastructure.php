<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scope;

class CloudAndInfrastructure extends Model
{
    public $timestamps = false;
    protected $table = 'cloud_and_infrastructures';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'created_at',
        'updated_at'
    ];

    /**
     * Get the scope associated with the cloud and infrastructure.
     */
    public function scope()
    {
        return $this->hasOne(Scope::class);
    }
}
