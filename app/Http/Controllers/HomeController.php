<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Scope;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $id = Auth::user()->id;
        $scopes = Scope::where('user_id', $id)
        ->with('cloud', 'cms', 'database', 'framework', 'language', 'type')
        ->get();
        return view('home', ['scopes' => $scopes]);
    }
}
