<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $this->validate($request, [
            'name' => 'required',
            'username' => 'required | unique:users,username,'.$user->id,
            'email' => 'required | email | unique:users,email,'.$user->id
                ], [
            'name.required' => 'Name is required.',
            'username.required' => 'This User name is required.',
            'username.unique' => 'This user name is already exists',
            'email.required' => 'Email is required',
            'email.email' => 'It should be an valid email address',
            'email.unique' => 'This email address is already exists.'
        ]);


        $data = array(
            'name' => $request->input('name'),            
            'username' => $request->input('username'),
            'email' => $request->input('email'),
            'updated_at' => date('Y-m-d')
        ); 

       
        $result = User::where('id', $id)->update($data);
        
        if($result){
            Toastr::success('Profile has successfully updated.', 'success');
            return redirect()->back();
        }else{
            Toastr::warning('W00ps! Something went wrong. Try again.', 'warning');
            return redirect()->back();
        }
    }


    public function update_password(Request $request, $id){
        $user = User::findOrFail($id);

        $this->validate($request, [
            'oldpassword' => 'required',
            'newpassword' => 'required',
            'confpassword' => 'required | same:newpassword'
                ], [
            'oldpassword.required' => 'Current password is required.',
            'newpassword.required' => 'New password is required.',
            'confpassword.required' => 'Confirm password is required',
            'confpassword.same' => 'Confirm password is not match with new password'
        ]);
        
        $uipass = $request->input('oldpassword');
        $newPass = $request->input('newpassword');
        $dbpass = $user->password;

        if (Hash::check($uipass, $dbpass)) {
            $hash_pass = password_hash($newPass, PASSWORD_DEFAULT);
            $updateResult = User::where('id', $id)->update(['updated_at' => date('Y-m-d'), 'password' => $hash_pass]);
            
            if($updateResult){
                Toastr::success('Password has successfully changed.', 'success');
                return redirect()->back();
            }else{
                Toastr::warning('W00ps! Something went wrong. Try again.', 'warning');
                return redirect()->back();
            }
        }else{
            Toastr::warning('W00ps! Current password wrong. Try again.', 'warning');
            return redirect()->back();
        }
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
