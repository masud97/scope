<div class="page-header-inner ">
<!-- BEGIN LOGO -->
<div class="page-logo">
    <a href="{{route('home')}}">
        <p>BEETLES LOGO</p>
        <!-- <img src="{{url('assets/layouts/layout/img/logo.png')}}" alt="logo" class="logo-default" /> </a> -->
</div>
<!-- END LOGO -->

<!-- BEGIN HEADER SEARCH BOX -->

<!-- BEGIN RESPONSIVE MENU TOGGLER -->
<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
    <span></span>
</a>
<!-- END RESPONSIVE MENU TOGGLER -->
<!-- BEGIN TOP NAVIGATION MENU -->
<div class="top-menu">
    <ul class="nav navbar-nav pull-right">
        
        <!-- BEGIN USER LOGIN DROPDOWN -->
        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
        <li class="dropdown dropdown-user">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <!-- <img alt="" class="img-circle" src="{{url('assets/layouts/layout/img/avatar3_small.jpg')}}" /> -->
                <span class="username username-hide-on-mobile"> {{Auth::user()->username}} </span>
                <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-default">
                @if(Auth::user()->hasVerifiedEmail())
                    <li>
                        <a data-toggle="modal" href="#profile">
                            <i class="icon-user"></i> My Profile </a>
                    </li>
                
                    <li>
                        <a data-toggle="modal" href="#changePassword">
                            <i class="icon-key"></i> Change Password </a>
                    </li>
                @endif
                <li class="divider"> </li>
                
                <li>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">               
                        <i class="icon-key"></i> Log Out </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </li>
        <!-- END USER LOGIN DROPDOWN -->
        
    </ul>

</div>
<!-- END TOP NAVIGATION MENU -->
</div>
