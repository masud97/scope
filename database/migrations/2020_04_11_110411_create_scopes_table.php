<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScopesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scopes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index()->nullable();
            $table->unsignedBigInteger('type_id')->index();
            $table->string('identifier');
            $table->unsignedBigInteger('language_id')->index()->nullable();
            $table->unsignedBigInteger('framework_id')->index()->nullable();
            $table->unsignedBigInteger('cloud_id')->index()->nullable();
            $table->unsignedBigInteger('database_id')->index()->nullable();
            $table->unsignedBigInteger('cms_id')->index()->nullable();
            $table->string('instructions')->nullable();
            $table->string('reference')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->tinyInteger('status')->default('1')->nullable();            

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('type_id')->references('id')->on('types');
            $table->foreign('language_id')->references('id')->on('languages');
            $table->foreign('framework_id')->references('id')->on('frameworks');
            $table->foreign('cloud_id')->references('id')->on('cloud_and_infrastructures');
            $table->foreign('database_id')->references('id')->on('databases');
            $table->foreign('cms_id')->references('id')->on('content_management_systems');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scopes');
    }
}
