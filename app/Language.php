<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Language;
use App\Framework;

class Language extends Model
{
    public $timestamps = false;
    protected $table = 'languages';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'created_at',
        'updated_at'
    ];

    /**
     * Get the scope associated with the Language.
     */
    public function scope()
    {
        return $this->hasOne(Scope::class);
    }

    /**
     * Get the frameworks associated with the Language.
     */
    public function frameworks()
    {
        return $this->hasMany(Framework::class);
    }

}


