<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scope;
use App\Language;

class Framework extends Model
{
    public $timestamps = false;
    protected $table = 'frameworks';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'created_at',
        'updated_at'
    ];

    /**
     * Get the scope associated with the Framework.
     */
    public function scope()
    {
        return $this->hasOne(Scope::class);
    }

    /**
     * Get the language associated with the Framework.
     */
    public function language()
    {
        return $this->hasOne(Language::class);
    }
}
