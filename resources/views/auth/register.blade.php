@extends('layouts.login-registration')
@section('title', 'Register')
@section('content')
<!-- BEGIN REGISTRATION FORM -->
<form class="register" action="{{ route('register') }}" method="post">
    @csrf
    <h3 class="font-green">Sign Up</h3>
    <p class="hint"> Enter your personal details below: </p>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Full Name</label>
        <input id="name" class="form-control placeholder-no-fix @error('name') is-invalid @enderror" type="text" placeholder="Full Name" name="name" value="{{ old('name') }}" required autocomplete="name"/>
        @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>

    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">User Name</label>
        <input id="username" class="form-control placeholder-no-fix @error('username') is-invalid @enderror" type="text" placeholder="User Name" name="username" value="{{ old('username') }}" required/> 
        <p class="hint">Username can only contain small letters and numbers like abc123.</p>
        @error('username')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>

    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Email</label>
        <input id="email" class="form-control placeholder-no-fix @error('email') is-invalid @enderror" type="text" placeholder="Email" name="email" value="{{ old('email') }}" required autocomplete="email"/>
        @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>    

    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Password</label>
        <input id="password" class="form-control placeholder-no-fix  @error('password') is-invalid @enderror" type="password" autocomplete="off" id="register_password" placeholder="Password" name="password" required/>
        @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
        </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
        <input id="password_confirmation" class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Re-type Your Password" name="password_confirmation" required/> 
        
    </div>
    <div class="form-group margin-top-20 margin-bottom-20">
        <label class="mt-checkbox mt-checkbox-outline">
            <input type="checkbox" name="tnc" required="required"/> I agree to the
            <a href="javascript:;">Terms of Service </a> &
            <a href="javascript:;">Privacy Policy </a>
            <span></span>
        </label>
        <div id="register_tnc_error"> </div>
    </div>
    <div class="form-actions">
        <button type="button" id="register-back-btn" class="btn green btn-outline">Back</button>
        <button type="submit" name="submit" id="register-submit-btn" class="btn btn-success uppercase pull-right">Submit</button>
    </div>
    <div class="create-account">
        <p>
        <label>
            Already I have an account
        </label>
            <a href="{{ route('login') }}" id="register-btn" class="uppercase">Login</a>
        </p>
    </div>
</form>
<!-- END REGISTRATION FORM -->
@endsection
