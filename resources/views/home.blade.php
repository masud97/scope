@extends('layouts.user')
@section('title', 'Scope')
@section('content')
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">            
            <li>
                <span>Scope</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN MAIN CONTENT -->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered" style="margin-top:15px;">
                
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="btn-group pull-right">
                                    <button data-toggle="modal" href="#basic" class="btn sbold green" onclick="showTypes(); showLanguages(); showCloudInfrastructure(); showDatabase(); showCMS();"> New Scope
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                        <thead>
                            <tr>
                                <th> Sl. </th>
                                <th> Type </th>
                                <th> Identifier </th>
                                <th> Language </th>
                                <th> Database </th>
                                <th> Actions </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($scopes as $key=>$scope)
                            <tr class="odd gradeX">
                                <td>{{ $key + 1 }}</td>
                                <td> {{ $scope->type->name }} </td>
                                <td>{{ $scope->identifier }}</td>
                                <td>
                                    @if(!empty($scope->language->name))
                                    {{ $scope->language->name }}
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($scope->database->name)) 
                                    {{ $scope->database->name }} 
                                    @endif
                                </td>
                                <td>
                                <a href="javascript:;" onclick="scopeDetails({{ $scope->id }});" title="Details" data-placement="top" data-toggle="tooltip" data-original-title="Details" class="btn btn-xs btn-success tooltips"><i class="fa fa-eye"></i></a>
                                <a href="javascript:;" onclick="scopeUpdate({{ $scope->id }});" title="Update" data-placement="top" data-toggle="tooltip" data-original-title="Update" class="btn btn-xs blue tooltips"><i class="fa fa-edit"></i></a>
                                <a href="javascript:;" onclick="event.preventDefault(); deleteScope({{ $scope->id }});" title="Delete" data-placement="top" data-toggle="tooltip" data-original-title="Delete" class="btn btn-xs red tooltips"><i class="fa fa-trash"></i></a>
                                <form id="delete-scope-{{ $scope->id }}" action="{{route('scope.destroy',$scope->id)}}" method="POST" style="display: none;">
                                    @csrf
                                </form>                                
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add New Scope</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE FORM PORTLET-->
                        <div class="portlet light bordered">                                    
                            <div class="portlet-body form">
                                <form role="form" action="{{route('new.scope')}}" method="POST">
                                    @csrf
                                    <div class="form-body">
                                    <div class="form-group">
                                        <label>Type <span class="required" aria-required="true"> * </span></label>
                                        <select class="form-control types" name="type" id="type" rqequred="required">
                                            
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Identifier <span class="required" aria-required="true"> * </span></label>                                                
                                        <input type="text" name="identifier" class="form-control" placeholder="e.g. www.example.com" required="required">
                                    </div>

                                    <div class="form-group">
                                        <label>Coding Language</label>
                                        <div class="input-icon">
                                            <i class="fa fa-search"></i>                                        
                                            <select name="language" class="form-control codingLanguages" id="codingLanguage">
                                            
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Framework</label>
                                        <div class="input-icon">
                                            <i class="fa fa-search"></i>                                        
                                            <select name="framework" class="form-control frameworks" id="framework">                                        
                                            
                                            </select> 
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Cloud and Infrastructure</label>
                                        <div class="input-icon">
                                            <i class="fa fa-search"></i>
                                            <select name="cloud" class="form-control cloudAndInfrastructures" id="cloudAndInfrastructure">
                                            
                                        </select> </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Database</label>
                                        <div class="input-icon">
                                            <i class="fa fa-search"></i>                                        
                                            <select name="database" class="form-control databases" id="database">
                                            
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Content Management System</label>
                                        <div class="input-icon">
                                            <i class="fa fa-search"></i>
                                            <select name="cms" class="form-control cms" id="cms">
                                            
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Instructions</label>
                                        <textarea name="instructions" id="instructions" class="form-control" rows="3"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label>Reference</label>                                                
                                        <input type="text" name="reference" id="reference" class="form-control" placeholder="">
                                    </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" name="submit" class="btn blue">Save</button>
                                        <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->                                
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- END MAIN CONTENT -->
</div>
@endsection
