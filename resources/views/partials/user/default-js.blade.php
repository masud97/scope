<!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<script src="../assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="{{url('assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{url('assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{url('assets/global/plugins/js.cookie.min.js')}}" type="text/javascript"></script>
<script src="{{url('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
<script src="{{url('assets/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
<script src="{{url('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
<script src="{{url('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
<script src="{{url('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
<script src="{{url('assets/pages/scripts/bootstrap-multiselect.js')}}" type="text/javascript"></script>

<script src="{{url('assets/global/scripts/app.min.js')}}" type="text/javascript"></script>
<script src="{{url('assets/pages/scripts/table-datatables-managed.js')}}" type="text/javascript"></script>
<script src="{{url('assets/layouts/layout/scripts/layout.min.js')}}" type="text/javascript"></script>
<script src="{{url('assets/layouts/layout/scripts/demo.min.js')}}" type="text/javascript"></script>
<script src="{{url('assets/layouts/global/scripts/quick-sidebar.min.js')}}" type="text/javascript"></script>
<script src="{{url('assets/layouts/global/scripts/quick-nav.min.js')}}" type="text/javascript"></script>
<script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
        {!! Toastr::message() !!}

        <script>
        @if($errors->any())
                @foreach($errors->all() as $error)
                toastr.error('{{ $error }}', 'Error', {
                        "closeButton": true,
                        "progressBar": true
                });
                @endforeach
        @endif
        </script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.10.12/dist/sweetalert2.all.min.js"></script>

<!-- END THEME LAYOUT SCRIPTS --> 

<script type="text/javascript">
    $(document).ready(function() {
        $('.multiselect-option').multiselect({
        buttonWidth: '100%',
        enableClickableOptGroups: true
    });
    });

    function showTypes(){
        url = "{{route('type.list')}}";
        $.ajax({
        url: url,
        method: "GET",
        }).done(function (data) {
            var $type = $('.types');
            $type.empty();        
            for (var i = 0; i < data.types.length; i++) {
                $type.append('<option value='+data.types[i]['id']+'>'+data.types[i]['name']+'</option>');
            }   
        });
    };

    function showLanguages() {
        url = "{{ route('language.list')}}";

        $.ajax({
        url: url,
        method: "GET",
        }).done(function (data) {
            var $codingLanguage = $('.codingLanguages');
            $codingLanguage.empty();
            $codingLanguage.append('<option value="">Select language</option>');
            for (var i = 0; i < data.languages.length; i++) {
                $codingLanguage.append('<option value='+data.languages[i]['id']+'>'+data.languages[i]['name']+'</option>');
            }   
        });
    };

    function showCloudInfrastructure(){
        url = "{{ route('cloud.list')}}";
        $.ajax({
        url: url,
        method: "GET",
        }).done(function (data) {
            var $cloudAndInfrastructure = $('.cloudAndInfrastructures');
            $cloudAndInfrastructure.empty();   
            $cloudAndInfrastructure.append('<option value="">Not selected</option>');     
            for (var i = 0; i < data.clouds.length; i++) {
                $cloudAndInfrastructure.append('<option value='+data.clouds[i]['id']+'>'+data.clouds[i]['name']+'</option>');
            }   
        });
    };


    function showDatabase(){
        url = "{{ route('database.list')}}";
        $.ajax({
        url: url,
        method: "GET",
        }).done(function (data) {
            var $database = $('.databases');
            $database.empty();  
            $database.append('<option value="">Not selected</option>');      
            for (var i = 0; i < data.databases.length; i++) {
                $database.append('<option value='+data.databases[i]['id']+'>'+data.databases[i]['name']+'</option>');
            }   
        });
    };

    function showCMS(){
        url = "{{ route('cms.list')}}";
        $.ajax({
        url: url,
        method: "GET",
        }).done(function (data) {
            var $cms = $('.cms');
            $cms.empty();  
            $cms.append('<option value="">Not selected</option>');      
            for (var i = 0; i < data.cms.length; i++) {
                $cms.append('<option value='+data.cms[i]['id']+'>'+data.cms[i]['name']+'</option>');
            }   
        });
    };


    function hideProfileModal(){
        $('#profile').modal('hide');
    };

    function scopeDetails(id){
        url = "{{ url('scope/details')}}/" +id;
        $.ajax({
        url: url,
        method: "GET",
        }).done(function (data) {
            $('#scopeDetails').modal('show');

            var $showType = $('#showType');
            $showType.empty();
            $showType.append(data.details.type['name']);

            var $showIdentifier = $('#showIdentifier');
            $showIdentifier.empty();
            $showIdentifier.append(data.details['identifier']);

            var $showLanguage = $('#showLanguage');
            $showLanguage.empty();
            $showLanguage.append(data.details.language['name']);

            var $showFramework = $('#showFramework');
            $showFramework.empty();
            $showFramework.append(data.details.framework['name']);

            var $showCloud = $('#showCloud');
            $showCloud.empty();
            $showCloud.append(data.details.cloud['name']);

            var $showDatabase = $('#showDatabase');
            $showDatabase.empty();
            $showDatabase.append(data.details.database['name']);

            var $showCMS = $('#showCMS');
            $showCMS.empty();
            $showCMS.append(data.details.cms['name']);

            var $showInstructions = $('#showInstructions');
            $showInstructions.empty();
            $showInstructions.append(data.details['instructions']);

            var $showReference = $('#showReference');
            $showReference.empty();
            $showReference.append(data.details['reference']);

        });
    }

    function scopeUpdate(id){
        url = "{{ url('scope/update')}}/" +id;
        $.ajax({
        url: url,
        method: "GET",
        }).done(function (data) {
            $('#scopeUpdate').modal('show');
            console.log(data);

            var $scopeId = $('#scopeId');
            $scopeId.empty();               
            $scopeId.append('<input type="hidden" name="scopeid" value='+data.details['id']+' class="form-control" required="required">');

            var $typeInUpdate = $('#typeInUpdate');
            $typeInUpdate.empty();               
            for (var i = 0; i < data.allTypes.length; i++) {
                if(data.details['type_id'] == data.allTypes[i]['id']){
                    $typeInUpdate.append('<option selected value='+data.allTypes[i]['id']+'>'+data.allTypes[i]['name']+'</option>');
                }else{
                    $typeInUpdate.append('<option value='+data.allTypes[i]['id']+'>'+data.allTypes[i]['name']+'</option>');
                }                
            } 

            var $identifierInUpdate = $('#identifierInUpdate');
            $identifierInUpdate.empty();               
            $identifierInUpdate.append('<input type="text" name="identifier" value='+data.details['identifier']+' class="form-control" required="required">');

            var $codingLanguageInUpdate = $('#codingLanguageInUpdate');
            $codingLanguageInUpdate.empty();
            if(!data.details['language_id']){
                $codingLanguageInUpdate.append('<option value="">Not selected</option>');  
            }                 
            for (var i = 0; i < data.allLanguages.length; i++) {
                if(data.details['language_id'] == data.allLanguages[i]['id']){
                    $codingLanguageInUpdate.append('<option selected value='+data.allLanguages[i]['id']+'>'+data.allLanguages[i]['name']+'</option>');
                }else{
                    $codingLanguageInUpdate.append('<option value='+data.allLanguages[i]['id']+'>'+data.allLanguages[i]['name']+'</option>');
                }                
            } 

            var $frameworkInUpdate = $('#frameworkInUpdate');
            $frameworkInUpdate.empty(); 
            if(!data.details['framework_id']){
                $frameworkInUpdate.append('<option value="">Not selected</option>');  
            }                           
            for (var i = 0; i < data.frameworks.length; i++) {
                if(data.details['framework_id'] == data.frameworks[i]['id']){
                    $frameworkInUpdate.append('<option selected value='+data.frameworks[i]['id']+'>'+data.frameworks[i]['name']+'</option>');
                }else{
                    $frameworkInUpdate.append('<option value='+data.frameworks[i]['id']+'>'+data.frameworks[i]['name']+'</option>');
                }                
            } 

            var $cloudAndInfrastructureInUpdate = $('#cloudAndInfrastructureInUpdate');
            $cloudAndInfrastructureInUpdate.empty();
            if(!data.details['cloud_id']){
                $cloudAndInfrastructureInUpdate.append('<option value="">Not selected</option>');  
            }               
            for (var i = 0; i < data.allClouds.length; i++) {
                if(data.details['cloud_id'] == data.allClouds[i]['id']){
                    $cloudAndInfrastructureInUpdate.append('<option selected value='+data.allClouds[i]['id']+'>'+data.allClouds[i]['name']+'</option>');
                }else{
                    $cloudAndInfrastructureInUpdate.append('<option value='+data.allClouds[i]['id']+'>'+data.allClouds[i]['name']+'</option>');
                }                
            } 

            var $databaseInUpdate = $('#databaseInUpdate');
            $databaseInUpdate.empty(); 
            if(!data.details['database_id']){
                $databaseInUpdate.append('<option value="">Not selected</option>');  
            }              
            for (var i = 0; i < data.allDatabases.length; i++) {
                if(data.details['database_id'] == data.allDatabases[i]['id']){
                    $databaseInUpdate.append('<option selected value='+data.allDatabases[i]['id']+'>'+data.allDatabases[i]['name']+'</option>');
                }else{
                    $databaseInUpdate.append('<option value='+data.allDatabases[i]['id']+'>'+data.allDatabases[i]['name']+'</option>');
                }                
            }            

            var $cmsInUpdate = $('#cmsInUpdate');
            $cmsInUpdate.empty();   
            if(!data.details['cms_id']){
                $cmsInUpdate.append('<option value="">Not selected</option>');  
            }            
            for (var i = 0; i < data.allCMS.length; i++) {
                if(data.details['cms_id'] == data.allCMS[i]['id']){
                    $cmsInUpdate.append('<option selected value='+data.allCMS[i]['id']+'>'+data.allCMS[i]['name']+'</option>');
                }else{
                    $cmsInUpdate.append('<option value='+data.allCMS[i]['id']+'>'+data.allCMS[i]['name']+'</option>');
                }                
            }


            var $instructionsInUpdate = $('#instructionsInUpdate');
            $instructionsInUpdate.empty();               
            $instructionsInUpdate.append('<textarea name="instructions" id="instructionsInUpdate" class="form-control" rows="3">'+data.details['instructions']+'</textarea>');

            var $referenceInUpdate = $('#referenceInUpdate');
            $referenceInUpdate.empty();               
            $referenceInUpdate.append('<input type="text" name="reference" value='+data.details['reference']+' class="form-control" required="required">');

        });
    };

</script>


<script>
    $('#codingLanguage').change(function () {
        var selectedLanguage = $('#codingLanguage').val();
        var url = "{{url('/framework-list')}}/" + selectedLanguage;
//        console.log(url);
        $.ajax({
            url: url,
            method: "GET",
        }).done(function (data) {
            var $framework = $('.frameworks');
            $framework.empty();
            $framework.append('<option value="">Not selected</option>');
            for (var i = 0; i < data.frameworks.length; i++) {
                $framework.append('<option value=' + data.frameworks[i]['id'] + '>' + data.frameworks[i]['name'] + '</option>');
            }
        });
    });
</script>

<script>
    $('#codingLanguageInUpdate').change(function () {
        var selectedLanguage = $('#codingLanguageInUpdate').val();
        var url = "{{url('/framework-list')}}/" + selectedLanguage;
//        console.log(url);
        $.ajax({
            url: url,
            method: "GET",
        }).done(function (data) {
            var $framework = $('.frameworks');
            $framework.empty();
            $framework.append('<option value="">Not selected</option>');
            for (var i = 0; i < data.frameworks.length; i++) {
                $framework.append('<option value=' + data.frameworks[i]['id'] + '>' + data.frameworks[i]['name'] + '</option>');
            }
        });
    });
</script>

<script>
    function deleteScope(id){

        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: true
            })

            swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
            }).then((result) => {
            if (result.value) {
                document.getElementById('delete-scope-'+id).submit();
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                'Cancelled',
                'Your imaginary file is safe :)',
                'error'
                )
            }
            })



    }
</script>