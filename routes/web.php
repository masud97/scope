<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

Route::post('/new/scope', 'ScopeController@store')->name('new.scope');
Route::get('/type-list', 'TypeController@index')->name('type.list');
Route::get('/language-list', 'LanguageController@index')->name('language.list');
Route::get('/cloud-list', 'CloudInfrastructureController@index')->name('cloud.list');
Route::get('/database-list', 'DatabaseController@index')->name('database.list');
Route::get('/cms-list', 'CMSController@index')->name('cms.list');
Route::get('/framework-list/{id}', 'FrameworkController@framework_list')->name('framework.list');

Route::post('/user/profile/update/{id}', 'UserController@update')->name('user.profile.update');
Route::post('/user/change/password/{id}', 'UserController@update_password')->name('user.change.password');

Route::get('/scope/details/{id}', 'ScopeController@show')->name('scope.details');
Route::get('/scope/update/{id}', 'ScopeController@edit')->name('scope.update');
Route::post('/scope/update', 'ScopeController@update')->name('scope.update');
Route::post('/scope/destroy/{id}', 'ScopeController@destroy')->name('scope.destroy');
